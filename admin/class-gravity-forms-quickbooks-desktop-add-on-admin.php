<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://parenthesis.io
 * @since      1.0.0
 *
 * @package    Gravity_Forms_Quickbooks_Desktop_Add_On
 * @subpackage Gravity_Forms_Quickbooks_Desktop_Add_On/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Gravity_Forms_Quickbooks_Desktop_Add_On
 * @subpackage Gravity_Forms_Quickbooks_Desktop_Add_On/admin
 * @author     Leroy Ley Loredo <leroy@parenthesis.io>
 */
class Gravity_Forms_Quickbooks_Desktop_Add_On_Admin
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $plugin_name The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $version The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string $plugin_name The name of this plugin.
     * @param      string $version The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Gravity_Forms_Quickbooks_Desktop_Add_On_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Gravity_Forms_Quickbooks_Desktop_Add_On_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/gravity-forms-quickbooks-desktop-add-on-admin.css', array(), $this->version, 'all');

    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Gravity_Forms_Quickbooks_Desktop_Add_On_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Gravity_Forms_Quickbooks_Desktop_Add_On_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/gravity-forms-quickbooks-desktop-add-on-admin.js', array('jquery'), $this->version, false);

    }

    /**
    * Add an options page under the Settings submenu
     *
    * @since  1.0.0
    */
    public function add_options_page()
    {
        add_menu_page(
            'Gravity Forms Quickbooks-Desktop Add on', // $page_title
            'GF-QBD Add on', // $menu_title
            'manage_options', // $capability
            $this->plugin_name, // $menu_slug
            array($this, 'display_options_page'), // $function
            '', // $icon_url
            '100.0' // $position (position number on menu from Top)
        );
    }

    public function ajax_post_data_web_connector()
    {
        global $wpdb;

        $table = $wpdb->prefix . 'gfqb_qwc';
        $wc = isset($_POST['web_connector']) ? (int) $_POST['web_connector'] : 0;

        $data = $wpdb->get_row("SELECT * FROM $table WHERE id = $wc");

        wp_send_json($data);
    }

    public function send_post_data_web_connector_submit()
    {
        global $wpdb;

        $table = $wpdb->prefix . 'gfqb_qwc';

        $web_connectors = (int) $_POST['web_connectors'];
        $server_name = $_POST['server_name'];
        $server_desc = $_POST['server_desc'];
        $server_url = $_POST['server_url'];
        $server_support_url = $_POST['server_support_url'];
        $server_username = $_POST['server_username'];
        $server_file_id = $_POST['server_file_id'];
        $server_owner_id = $_POST['server_owner_id'];
        $server_readonly = (int) $_POST['server_readonly'];
        $server_update_frecuency = (int) $_POST['server_update_frecuency'];

        $wpdb->update(
            $table,
            array(
                'server_name' => $server_name,
                'server_desc' => $server_desc,
                'server_url' => $server_url,
                'server_support_url' => $server_support_url,
                'server_username' => $server_username,
                'server_file_id' => $server_file_id,
                'server_owner_id' => $server_owner_id,
                'server_readonly' => $server_readonly,
                'server_update_frecuency' => $server_update_frecuency
            ),
            array(
                'id' => $web_connectors
            ),
            null,
            array( '%d' )
        );

        wp_redirect('admin.php?page=gravity-forms-quickbooks-desktop-add-on');
    }

    public function send_post_data_quickbooks_db_submit()
    {
        global $wpdb;

        $table = $wpdb->prefix . 'gfqb_qbdbsettings';

        $dbid = $_POST['dbid'];
        $dbtype = $_POST['dbtype'];
        $dbserver = $_POST['dbserver'];
        $dbname = $_POST['dbname'];
        $dbuser = $_POST['dbuser'];
        $dbpass = $_POST['dbpass'];

        $wpdb->update(
            $table,
            array(
                'dbtype' => $dbtype,
                'dbserver' => $dbserver,
                'dbname' => $dbname,
                'dbuser' => $dbuser,
                'dbpass' => $dbpass
            ),
            array(
                'id' => $dbid
            ),
            null,
            array( '%d' )
        );

        wp_redirect('admin.php?page=gravity-forms-quickbooks-desktop-add-on');
    }

    /**
     * Render the options page for plugin
     *
     * @since  1.0.0
     */
    public function display_options_page() {
        include_once 'partials/gravity-forms-quickbooks-desktop-add-on-admin-display.php';
    }
}

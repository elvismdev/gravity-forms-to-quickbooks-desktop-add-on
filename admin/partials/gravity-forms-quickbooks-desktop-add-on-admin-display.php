<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://parenthesis.io
 * @since      1.0.0
 *
 * @package    Gravity_Forms_Quickbooks_Desktop_Add_On
 * @subpackage Gravity_Forms_Quickbooks_Desktop_Add_On/admin/partials
 */

/** WordPress Administration Bootstrap */
require_once(ABSPATH . 'wp-admin/admin.php');

/** WordPress Translation Install API */
require_once(ABSPATH . 'wp-admin/includes/translation-install.php');

if (!current_user_can('manage_options'))
    wp_die(__('You do not have sufficient permissions to manage options for this site.'));

$title = __('Gravity Forms Quickbooks-Desktop Add on');

global $wpdb;
$all_set = true;

try {
    // Quickbooks database settings
    $qbdbsettings = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "gfqb_qbdbsettings LIMIT 1");

    // Web connectors
    $web_connectors = $wpdb->get_results("SELECT id, endpoint_name FROM " . $wpdb->prefix . "gfqb_qwc GROUP BY endpoint_name ORDER BY id");
} catch (Exception $e) {
    $all_set = false;
}

echo '<div class="wrap">
        <div class="row">
            <h1>' . esc_html($title) .'</h1>
        </div>
';

if (!$all_set || !$qbdbsettings || !$web_connectors) {
    echo 'Internal error ocurred with plugin. Maybe some required tables are missing.';
} else { ?>

    <!-- Latest compiled and minified CSS -->
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">-->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"
            type="application/javascript"></script>

    <script>
        $(document).ready(function () {
            $('#web_connectors').change(function () {
                var jqxhr = $.post("admin-post.php", {
                    action: 'data_web_connector',
                    web_connector: $('#web_connectors').val(),
                    dataType: 'json', // Choosing a JSON datatype
                })
                    .done(function (data) {
                        $('#server_name').val(data.server_name);
                        $('#server_desc').val(data.server_desc);
                        $('#server_url').val(data.server_url);
                        $('#server_support_url').val(data.server_support_url);
                        $('#server_username').val(data.server_username);
                        $('#server_file_id').val(data.server_file_id);
                        $('#server_owner_id').val(data.server_owner_id);
                        $('#server_readonly').val(data.server_readonly);
                        $('#server_update_frecuency').val(data.server_update_frecuency);
                        $('#form_step_2').css('visibility', 'visible');
                    })
                    .fail(function () {
                    })
                    .always(function () {
                    });
            });
        });
    </script>

    <div class="row">
        <h2><?php echo esc_html('Quickbooks database settings'); ?></h2>

        <form method="post" action="admin-post.php?action=data_quickbooks_db_submit">
            <fieldset>
                <div class="form-group">
                    <label for="dbtype">Database type</label>
                    <select name="dbtype" id="dbtype">
                        <option>Select database</option>
                        <option value="mysql" <?= ($qbdbsettings->dbtype == 'mysql') ? 'selected' : '' ?>>MySQL</option>
                        <option
                            value="pearmdb2.mysql" <?= ($qbdbsettings->dbtype == 'pearmdb2.mysql') ? 'selected' : '' ?>>
                            MySQL with PEAR MDB2 library
                        </option>
                        <option value="mssql" <?= ($qbdbsettings->dbtype == 'mssql') ? 'selected' : '' ?>>Microsoft
                            SQL
                        </option>
                        <option value="pgsql" <?= ($qbdbsettings->dbtype == 'pgsql') ? 'selected' : '' ?>>Postgre SQL
                        </option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="dbserver">Database server</label>
                    <input type="text" id="dbserver" name="dbserver" value="<?= $qbdbsettings->dbserver ?>">
                </div>
                <div class="form-group">
                    <label for="dbname">Database name</label>
                    <input type="text" id="dbname" name="dbname" value="<?= $qbdbsettings->dbname ?>">
                </div>
                <div class="form-group">
                    <label for="dbuser">Database username</label>
                    <input type="text" id="dbuser" name="dbuser" value="<?= $qbdbsettings->dbuser ?>">
                </div>
                <div class="form-group">
                    <label for="dbpass">Database password</label>
                    <input type="password" id="dbpass" name="dbpass" value="<?= $qbdbsettings->dbpass ?>">
                </div>
                <input type="hidden" id="dbid" name="dbid" value="<?= $qbdbsettings->id ?>">
                <div class="form-group">
                    <span class="spinner"></span>
                    <input name="original_publish" type="hidden" id="original_publish" value="Update">
                    <input name="save" type="submit" class="button button-primary button-large" id="publish"
                           value="Update">
            </fieldset>
        </form>
    </div>

    <div class="row">
        <h2><?php echo esc_html('Quickbooks SOAP service endpoint settings'); ?></h2>
    </div>

    <div class="row">
        <h2><?php echo esc_html('Quickbooks Desktop Connector settings'); ?></h2>

        <form method="post" action="admin-post.php?action=data_web_connector_submit">
            <fieldset id="form_step_1">
                <div class="form-group">
                    <label for="web_connectors">Endpoints configured</label>
                    <select name="web_connectors" id="web_connectors">
                        <option>Select endpoint</option>
                        <?php
                        foreach ($web_connectors as $wc) { ?>
                            <option value="<?= $wc->id ?>"><?= $wc->endpoint_name ?></option>
                        <?php }
                        ?>
                    </select>
                </div>
            </fieldset>

            <fieldset id="form_step_2" style="visibility: hidden">
                <div class="form-group">
                    <label for="server_name">Server name</label>
                    <input type="text" name="server_name" id="server_name" value="" size="50">
                </div>
                <div class="form-group">
                    <label for="server_desc">Server description</label>
                    <input type="text" name="server_desc" id="server_desc" value="" size="50">
                </div>
                <div class="form-group">
                    <label for="server_url">Server URL</label>
                    <input type="text" name="server_url" id="server_url" value="" size="50">
                </div>
                <div class="form-group">
                    <label for="server_support_url">Server support URL</label>
                    <input type="text" name="server_support_url" id="server_support_url" value=">" size="50">
                </div>
                <div class="form-group">
                    <label for="server_username">Server username</label>
                    <input type="text" name="server_username" id="server_username" value="" size="50">
                </div>
                <div class="form-group">
                    <label for="server_file_id">Server file ID</label>
                    <input type="text" name="server_file_id" id="server_file_id" value="" size="50">
                </div>
                <div class="form-group">
                    <label for="server_owner_id">Server owner ID</label>
                    <input type="text" name="server_owner_id" id="server_owner_id" value="" size="50">
                </div>
                <div class="form-group">
                    <label for="server_readonly">Server readonly</label>
                    <select name="server_readonly" id="server_readonly">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="server_update_frecuency">Server update frecuency</label>
                    <input type="text" name="server_update_frecuency" id="server_update_frecuency" value="" size="10">
                </div>
                <div class="form-group">
                    <span class="spinner"></span>
                    <input name="original_publish" type="hidden" id="original_publish" value="Update">
                    <input name="save" type="submit" class="button button-primary button-large" id="publish"
                           value="Update">
                </div>
            </fieldset>
        </form>
    </div>

<?php echo '</div>';
} ?>
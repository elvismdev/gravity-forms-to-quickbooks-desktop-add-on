<?php

/**
 * Generating QuickBooks *.QWC file
 * wp-content/plugins/gravity-forms-quickbooks-desktop-add-on/public/qbsoap_endpoints/generator_qwc.php
 */

// Error reporting... 
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);

/**
 * Require the utilities class
 */
require_once '../../vendor/consolibyte/quickbooks/QuickBooks.php';

/**
 * Require WP stuffs
 */
require_once '../../../../../wp-blog-header.php';
require_once '../../../../../wp-admin/includes/plugin.php';

global $wpdb;

// Web connector
$web_connector = 'web_connector.php';

// Check for table and plugin status
if ($wpdb->get_var('SHOW TABLES LIKE "' . $wpdb->prefix . 'gfqb_qwc"') != $wpdb->prefix . 'gfqb_qwc' || !is_plugin_active('gravity-forms-quickbooks-desktop-add-on/gravity-forms-quickbooks-desktop-add-on.php')) {
    echo 'Table needed for generate qwc file isn\'t exist or maybe you should check for plugin status';
    exit;
}

$table = $wpdb->prefix . 'gfqb_qwc';

$settings = $wpdb->get_row("SELECT * FROM $table WHERE endpoint_name = '$web_connector'");

$name = $settings->server_name;                // A name for your server (make it whatever you want)
$descrip = $settings->server_desc;        // A description of your server
$appurl = $settings->server_url;        // This *must* be httpS:// (path to your QuickBooks SOAP server)
$appsupport = $settings->server_support_url;        // This *must* be httpS:// and the domain name must match the domain name above
$username = $settings->server_username;        // This is the username you stored in the 'quickbooks_user' table by using QuickBooks_Utilities::createUser()
$fileid = $settings->server_file_id;        // Just make this up, but make sure it keeps that format
$ownerid = $settings->server_owner_id;        // Just make this up, but make sure it keeps that format
$qbtype = QUICKBOOKS_TYPE_QBFS;    // You can leave this as-is unless you're using QuickBooks POS
$readonly = (bool) $settings->server_readonly; // No, we want to write data to QuickBooks
$run_every_n_seconds = $settings->server_update_frecuency; // Run every 600 seconds (10 minutes)

// Generate the XML file
$QWC = new QuickBooks_WebConnector_QWC($name, $descrip, $appurl, $appsupport, $username, $fileid, $ownerid, $qbtype, $readonly, $run_every_n_seconds);
$xml = $QWC->generate();

// Send as a file download
header('Content-type: text/xml');
header('Content-Disposition: attachment; filename="quickbooks-wc-file.qwc"');
print($xml);
exit;

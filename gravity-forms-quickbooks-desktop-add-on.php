<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://parenthesis.io
 * @since             1.0.0
 * @package           Gravity_Forms_Quickbooks_Desktop_Add_On
 *
 * @wordpress-plugin
 * Plugin Name:       Gravity Forms Quickbooks Desktop Add On
 * Plugin URI:        https://parenthesis.io/gravity-forms-quickbooks-desktop-add-on
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Leroy Ley Loredo
 * Author URI:        https://parenthesis.io
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       gravity-forms-quickbooks-desktop-add-on
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-gravity-forms-quickbooks-desktop-add-on-activator.php
 */
function activate_gravity_forms_quickbooks_desktop_add_on() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gravity-forms-quickbooks-desktop-add-on-activator.php';
	Gravity_Forms_Quickbooks_Desktop_Add_On_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-gravity-forms-quickbooks-desktop-add-on-deactivator.php
 */
function deactivate_gravity_forms_quickbooks_desktop_add_on() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gravity-forms-quickbooks-desktop-add-on-deactivator.php';
	Gravity_Forms_Quickbooks_Desktop_Add_On_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_gravity_forms_quickbooks_desktop_add_on' );
register_deactivation_hook( __FILE__, 'deactivate_gravity_forms_quickbooks_desktop_add_on' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-gravity-forms-quickbooks-desktop-add-on.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_gravity_forms_quickbooks_desktop_add_on() {

	$plugin = new Gravity_Forms_Quickbooks_Desktop_Add_On();
	$plugin->run();

}
run_gravity_forms_quickbooks_desktop_add_on();

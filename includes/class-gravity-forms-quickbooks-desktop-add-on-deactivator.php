<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://parenthesis.io
 * @since      1.0.0
 *
 * @package    Gravity_Forms_Quickbooks_Desktop_Add_On
 * @subpackage Gravity_Forms_Quickbooks_Desktop_Add_On/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Gravity_Forms_Quickbooks_Desktop_Add_On
 * @subpackage Gravity_Forms_Quickbooks_Desktop_Add_On/includes
 * @author     Leroy Ley Loredo <leroy@parenthesis.io>
 */
class Gravity_Forms_Quickbooks_Desktop_Add_On_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		global $wpdb;

		$table_prefix = $wpdb->prefix . 'gfqb_';

		$tables = array('qwc', 'qbdbsettings');

		foreach ($tables as $table) {
			$sql = "DROP TABLE IF EXISTS `" . $table_prefix . "$table`;";
			$wpdb->query($sql);
		}
	}

}

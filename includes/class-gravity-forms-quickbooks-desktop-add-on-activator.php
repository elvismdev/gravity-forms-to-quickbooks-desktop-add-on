<?php

/**
 * Fired during plugin activation
 *
 * @link       https://parenthesis.io
 * @since      1.0.0
 *
 * @package    Gravity_Forms_Quickbooks_Desktop_Add_On
 * @subpackage Gravity_Forms_Quickbooks_Desktop_Add_On/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Gravity_Forms_Quickbooks_Desktop_Add_On
 * @subpackage Gravity_Forms_Quickbooks_Desktop_Add_On/includes
 * @author     Leroy Ley Loredo <leroy@parenthesis.io>
 */
class Gravity_Forms_Quickbooks_Desktop_Add_On_Activator
{

    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function activate()
    {
        global $wpdb;

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

        $table_prefix = $wpdb->prefix . 'gfqb_';
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE IF NOT EXISTS
        `" . $table_prefix . "qwc` (
        `id` INT NOT NULL AUTO_INCREMENT,
        `endpoint_name` VARCHAR(100) NOT NULL,
        `server_name` VARCHAR(100) NOT NULL,
        `server_desc` VARCHAR(100) NOT NULL,
        `server_url` VARCHAR(255) NOT NULL,
        `server_support_url` VARCHAR(255) NOT NULL,
        `server_username` VARCHAR(45) NOT NULL,
        `server_file_id` VARCHAR(36) NOT NULL,
        `server_owner_id` VARCHAR(36) NOT NULL,
        `server_readonly` tinyint(1) NOT NULL,
        `server_update_frecuency` int NOT NULL,
        PRIMARY KEY (`id`))
        " . $charset_collate . ";";

        dbDelta($sql);

        // Insert first qwc endpoint data
        $sql = "INSERT INTO `" . $table_prefix . "qwc` (`endpoint_name`, `server_name`, `server_desc`, `server_url`, `server_support_url`, `server_username`, `server_file_id`, `server_owner_id`, `server_readonly`, `server_update_frecuency`)
                VALUES (
                    'Test endpoint',
                    'My QuickBooks SOAP Server',
                    'An example QuickBooks SOAP Server',
                    'http://localhost/wordpress-master/wp-content/plugins/gravity-forms-quickbooks-desktop-add-on/public/qbsoap_endpoints/web_connector.php',
                    'http://localhost/wordpress-master/wp-content/plugins/gravity-forms-quickbooks-desktop-add-on/public/help',
                    'your-quickbooks-username',
                    '57F3B9B6-86F1-4FCC-B1FF-966DE1813D20',
                    '57F3B9B6-86F1-4FCC-B1FF-166DE1813D20',
                    0,
                    600
                );";

        dbDelta($sql);

        $sql = "CREATE TABLE IF NOT EXISTS
        `" . $table_prefix . "qbdbsettings` (
        `id` INT NOT NULL AUTO_INCREMENT,
        `dbtype` VARCHAR(20) NOT NULL,
        `dbuser` VARCHAR(100) NOT NULL,
        `dbpass` VARCHAR(255) NOT NULL,
        `dbserver` VARCHAR(255) NOT NULL,
        `dbname` VARCHAR(255) NOT NULL,
        PRIMARY KEY (`id`))
        " . $charset_collate . ";";

        dbDelta($sql);

        // Insert dump QuickBooks database settings
        $sql = "INSERT INTO `" . $table_prefix . "qbdbsettings` (`dbtype`, `dbuser`, `dbpass`, `dbserver`, `dbname`)
                VALUES (
                    'mysql',
                    'root',
                    'root',
                    'localhost',
                    'quickbooks_server'
                );";

        dbDelta($sql);
    }

}

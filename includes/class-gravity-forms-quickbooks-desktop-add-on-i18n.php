<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://parenthesis.io
 * @since      1.0.0
 *
 * @package    Gravity_Forms_Quickbooks_Desktop_Add_On
 * @subpackage Gravity_Forms_Quickbooks_Desktop_Add_On/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Gravity_Forms_Quickbooks_Desktop_Add_On
 * @subpackage Gravity_Forms_Quickbooks_Desktop_Add_On/includes
 * @author     Leroy Ley Loredo <leroy@parenthesis.io>
 */
class Gravity_Forms_Quickbooks_Desktop_Add_On_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'gravity-forms-quickbooks-desktop-add-on',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
